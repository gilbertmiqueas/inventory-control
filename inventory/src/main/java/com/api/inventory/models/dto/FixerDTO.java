package com.api.inventory.models.dto;

public class FixerDTO {

	public FixerDTO(Integer id, Integer size, Integer quantity, FixerTypeDTO fixerType) {
		super();
		this.id = id;
		this.size = size;
		this.quantity = quantity;
		this.fixerType = fixerType;
	}

	private Integer id;
	private Integer size;
	private Integer quantity;
	private FixerTypeDTO fixerType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public FixerTypeDTO getFixerType() {
		return fixerType;
	}

	public void setFixerType(FixerTypeDTO fixerType) {
		this.fixerType = fixerType;
	}

}
