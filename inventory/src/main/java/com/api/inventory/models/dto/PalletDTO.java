package com.api.inventory.models.dto;

public class PalletDTO {

	public PalletDTO(Integer id, Integer width, Integer length, Integer quantity, CompanyDTO company) {
		super();
		this.id = id;
		this.width = width;
		this.length = length;
		this.quantity = quantity;
		this.company = company;
	}

	private Integer id;
	private Integer width;
	private Integer length;
	private Integer quantity;
	private CompanyDTO company;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public CompanyDTO getCompany() {
		return company;
	}

	public void setCompany(CompanyDTO company) {
		this.company = company;
	}
}
