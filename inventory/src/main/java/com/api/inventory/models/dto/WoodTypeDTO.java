package com.api.inventory.models.dto;

import java.util.List;

public class WoodTypeDTO {
	
	public WoodTypeDTO(Integer id, String name, List<WoodDTO> woodList) {
		super();
		this.id = id;
		this.name = name;
		this.woodList = woodList;
	}

	private Integer id;
	private String name;
	private List<WoodDTO> woodList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WoodDTO> getWoodList() {
		return woodList;
	}

	public void setWoodList(List<WoodDTO> woodList) {
		this.woodList = woodList;
	}

}
