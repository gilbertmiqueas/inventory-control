package com.api.inventory.models.dto;

import java.util.Date;

public class ExitPalletDTO {

	public ExitPalletDTO(Integer id, Integer quantity, PalletDTO pallet, Date exitDate) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.pallet = pallet;
		this.exitDate = exitDate;
	}

	private Integer id;
	private Integer quantity;
	private PalletDTO pallet;
	private Date exitDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public PalletDTO getPallet() {
		return pallet;
	}

	public void setPallet(PalletDTO pallet) {
		this.pallet = pallet;
	}

	public Date getExitDate() {
		return exitDate;
	}

	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}
}
