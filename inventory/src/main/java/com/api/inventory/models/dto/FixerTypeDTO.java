package com.api.inventory.models.dto;

import java.util.List;

public class FixerTypeDTO {
	
	public FixerTypeDTO(Integer id, String name, List<FixerDTO> fixerList) {
		super();
		this.id = id;
		this.name = name;
		this.fixerList = fixerList;
	}

	private Integer id;
	private String name;
	private List<FixerDTO> fixerList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FixerDTO> getFixerList() {
		return fixerList;
	}

	public void setFixerList(List<FixerDTO> fixerList) {
		this.fixerList = fixerList;
	}

}
