package com.api.inventory.models;

import java.util.List;

public class ProductionRaw {

	private Production production;
	private List<Wood> woodList;
	private List<Fixer> fixerList;
	
	public Production getProduction() {
		return production;
	}
	
	public void setProduction(Production production) {
		this.production = production;
	}

	public List<Wood> getWoodList() {
		return woodList;
	}

	public void setWoodList(List<Wood> woodList) {
		this.woodList = woodList;
	}

	public List<Fixer> getFixerList() {
		return fixerList;
	}

	public void setFixerList(List<Fixer> fixerList) {
		this.fixerList = fixerList;
	}
	
}