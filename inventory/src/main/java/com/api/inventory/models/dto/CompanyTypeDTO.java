package com.api.inventory.models.dto;

import java.util.List;

public class CompanyTypeDTO {

	public CompanyTypeDTO(Integer id, String name, List<CompanyDTO> companyList) {
		super();
		this.id = id;
		this.name = name;
		this.companyList = companyList;
	}

	private Integer id;
	private String name;
	private List<CompanyDTO> companyList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CompanyDTO> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<CompanyDTO> companyList) {
		this.companyList = companyList;
	}

}