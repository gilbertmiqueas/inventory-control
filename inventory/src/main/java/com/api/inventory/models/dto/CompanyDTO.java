package com.api.inventory.models.dto;

public class CompanyDTO {

	public CompanyDTO(Integer id, String name, CompanyTypeDTO companyTypeDTO) {
		super();
		this.id = id;
		this.name = name;
		this.companyTypeDTO = companyTypeDTO;
	}

	private Integer id;
	private String name;
	private CompanyTypeDTO companyTypeDTO;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CompanyTypeDTO getCompanyTypeDTO() {
		return companyTypeDTO;
	}

	public void setCompanyTypeDTO(CompanyTypeDTO companyTypeDTO) {
		this.companyTypeDTO = companyTypeDTO;
	}

}
