package com.api.inventory.models.dto;

public class WoodDTO {

	public WoodDTO(Integer id, Integer width, Integer length, Integer thickness, Integer quantity,
			WoodTypeDTO woodType) {
		super();
		this.id = id;
		this.width = width;
		this.length = length;
		this.thickness = thickness;
		this.quantity = quantity;
		this.woodType = woodType;
	}

	private Integer id;
	private Integer width;
	private Integer length;
	private Integer thickness;
	private Integer quantity;
	private WoodTypeDTO woodType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getThickness() {
		return thickness;
	}

	public void setThickness(Integer thickness) {
		this.thickness = thickness;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public WoodTypeDTO getWoodType() {
		return woodType;
	}

	public void setWoodType(WoodTypeDTO woodType) {
		this.woodType = woodType;
	}

}
