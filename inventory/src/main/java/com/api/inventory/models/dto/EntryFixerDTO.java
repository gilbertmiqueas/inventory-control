package com.api.inventory.models.dto;

import java.util.Date;

public class EntryFixerDTO {

	public EntryFixerDTO(Integer id, Integer quantity, FixerDTO fixer, CompanyDTO company, Date entryDate) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.fixer = fixer;
		this.company = company;
		this.entryDate = entryDate;
	}

	private Integer id;
	private Integer quantity;
	private FixerDTO fixer;
	private CompanyDTO company;
	private Date entryDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public FixerDTO getFixer() {
		return fixer;
	}

	public void setFixer(FixerDTO fixer) {
		this.fixer = fixer;
	}

	public CompanyDTO getCompany() {
		return company;
	}

	public void setCompany(CompanyDTO company) {
		this.company = company;
	}
	
	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

}

