package com.api.inventory.models.dto;

import java.util.Date;

public class EntryWoodDTO {

	public EntryWoodDTO(Integer id, Integer quantity, WoodDTO wood, CompanyDTO company, Date entryDate) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.wood = wood;
		this.company = company;
		this.entryDate = entryDate;
	}

	private Integer id;
	private Integer quantity;
	private WoodDTO wood;
	private CompanyDTO company;
	private Date entryDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public WoodDTO getWood() {
		return wood;
	}

	public void setWood(WoodDTO wood) {
		this.wood = wood;
	}
	
	public CompanyDTO getCompany() {
		return company;
	}

	public void setCompany(CompanyDTO company) {
		this.company = company;
	}
	
	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

}
