package com.api.inventory.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.ProductionRaw;
import com.api.inventory.services.impl.ProductionServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/producao")
public class ProductionController {
	
	//services
	@Autowired
	private  ProductionServiceImpl productionServiceImpl;

	//lista todas as entradas
	//@RequestMapping(value="/todas", method=RequestMethod.GET)
	//public @ResponseBody List<EntryWoodDTO> listAll() {
		//return entryWooodServiceImpl.findAll();
	//}
	
	//salvar um material
	@RequestMapping(value="/nova", method=RequestMethod.POST)
	public @ResponseBody void saveNew(@RequestBody ProductionRaw productionRaw) {
		productionServiceImpl.save(productionRaw);
	}
}