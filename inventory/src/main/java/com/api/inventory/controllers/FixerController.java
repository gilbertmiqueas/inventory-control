package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.dto.FixerDTO;
import com.api.inventory.services.impl.FixerServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/fixador")
public class FixerController {
	
	//services
	@Autowired
	private FixerServiceImpl fixerServiceImpl;

	//lista todos os pallets
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<FixerDTO> listAll() {
		return fixerServiceImpl.findAll();
	}
}
