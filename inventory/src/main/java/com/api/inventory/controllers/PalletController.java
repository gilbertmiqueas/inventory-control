package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.dto.PalletDTO;
import com.api.inventory.services.impl.PalletServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/pallet")
public class PalletController {
	
	//services
	@Autowired
	private PalletServiceImpl palletServiceImpl;

	//lista todos os pallets
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<PalletDTO> listAll() {
		return palletServiceImpl.findAll();
	}
}
