package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.ExitPallet;
import com.api.inventory.models.dto.ExitPalletDTO;
import com.api.inventory.services.impl.ExitPalletServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/saida")
public class ExitPalletController {
	
	//services
	@Autowired
	private ExitPalletServiceImpl exitPalletServiceImpl;

	//lista todas as saidas de pallet
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<ExitPalletDTO> listAll() {
		return exitPalletServiceImpl.findAll();
	}
	
	//salvar um material
	@RequestMapping(value="/nova", method=RequestMethod.POST)
	public @ResponseBody void saveNew(@RequestBody ExitPallet exit) {
		exitPalletServiceImpl.save(exit);
	}
}