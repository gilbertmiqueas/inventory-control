package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.EntryWood;
import com.api.inventory.models.dto.EntryWoodDTO;
import com.api.inventory.services.impl.EntryWoodServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/entrada/madeira")
public class EntryWoodController {
	
	//services
	@Autowired
	private EntryWoodServiceImpl entryWooodServiceImpl;

	//lista todas as entradas
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<EntryWoodDTO> listAll() {
		return entryWooodServiceImpl.findAll();
	}
	
	//salvar um material
	@RequestMapping(value="/nova", method=RequestMethod.POST)
	public @ResponseBody void saveNew(@RequestBody EntryWood entry) {
		 entryWooodServiceImpl.save(entry);
	}
}