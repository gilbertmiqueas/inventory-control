package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.dto.WoodTypeDTO;
import com.api.inventory.services.impl.WoodTypeServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/tipo-madeira")
public class WoodTypeController {
	
	//services
	@Autowired
	private WoodTypeServiceImpl woodTypeServiceImpl;

	//lista todas as saidas de pallet
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<WoodTypeDTO> listAll() {
		return woodTypeServiceImpl.findAll();
	}
}
