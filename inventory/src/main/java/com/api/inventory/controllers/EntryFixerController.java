package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.EntryFixer;
import com.api.inventory.models.dto.EntryFixerDTO;
import com.api.inventory.services.impl.EntryFixerServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/entrada/fixador")
public class EntryFixerController {
	
	//services
	@Autowired
	private EntryFixerServiceImpl entryFixerServiceImpl;

	//lista todas as entradas
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<EntryFixerDTO> listAll() {
		return entryFixerServiceImpl.findAll();
	}
	
	//salvar um material
	@RequestMapping(value="/nova", method=RequestMethod.POST)
	public @ResponseBody void saveNew(@RequestBody EntryFixer entry) {
		entryFixerServiceImpl.save(entry);
	}
}