package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.dto.CompanyDTO;
import com.api.inventory.services.impl.CompanyServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/empresas")
public class CompanyController {
	
	//services
	@Autowired
	private CompanyServiceImpl companyServiceImpl;

	//lista todas as saidas de pallet
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<CompanyDTO> listAll() {
		return companyServiceImpl.findAll();
	}
}