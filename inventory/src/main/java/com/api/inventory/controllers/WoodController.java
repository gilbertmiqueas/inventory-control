package com.api.inventory.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.inventory.models.dto.WoodDTO;
import com.api.inventory.services.impl.WoodServiceImpl;

@RestController
@CrossOrigin()
@RequestMapping("/madeira")
public class WoodController {
	
	//services
	@Autowired
	private WoodServiceImpl woodServiceImpl;

	//lista todos os pallets
	@RequestMapping(value="/todas", method=RequestMethod.GET)
	public @ResponseBody List<WoodDTO> listAll() {
		return woodServiceImpl.findAll();
	}
}
