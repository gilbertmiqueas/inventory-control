package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.EntryFixer;

@Repository
public interface EntryFixerRepository extends CrudRepository<EntryFixer, Integer>{

	//lista todos os materiais
	List<EntryFixer> findAll();
}
