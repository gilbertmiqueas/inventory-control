package com.api.inventory.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.ProductionFixer;

@Repository
public interface ProductionFixerRepository extends CrudRepository<ProductionFixer, Integer> {
	
	//lista todos os materiais
	//List<FixerType> findAll();
}