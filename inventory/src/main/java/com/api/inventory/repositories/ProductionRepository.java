package com.api.inventory.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.Production;

@Repository
public interface ProductionRepository extends CrudRepository<Production, Integer> {
	
	//lista todos os materiais
	//List<FixerType> findAll();
}