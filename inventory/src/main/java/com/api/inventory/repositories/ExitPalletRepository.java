package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.ExitPallet;

@Repository
public interface ExitPalletRepository extends CrudRepository<ExitPallet, Integer>{

	//lista todos os materiais
	List<ExitPallet> findAll();
}
