package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.Wood;

@Repository
public interface WoodRepository extends CrudRepository<Wood, Integer> {
	
	@Query(value = "SELECT w FROM Wood w WHERE w.width = :width AND w.length = :length AND w.thickness = :thickness AND w.woodType.id = :type")
	Wood findBySpecifications(
			@Param("width") Integer width,
			@Param("length") Integer length,
			@Param("thickness") Integer thickness,
			@Param("type") Integer type);
	
	//lista todos as madeiras
	List<Wood> findAll();
}
