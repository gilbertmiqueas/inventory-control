package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.Pallet;

@Repository
public interface PalletRepository extends CrudRepository<Pallet, Integer>{

	@Query(value = "SELECT p FROM Pallet p WHERE p.width = :width AND p.length = :length AND p.company.id = :company")
	Pallet findBySpecifications(
			@Param("width") Integer width,
			@Param("length") Integer length,
			@Param("company") Integer company);
	
	//lista todos os materiais
	List<Pallet> findAll();
}
