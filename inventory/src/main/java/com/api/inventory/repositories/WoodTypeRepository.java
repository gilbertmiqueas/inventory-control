package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.WoodType;

@Repository
public interface WoodTypeRepository extends CrudRepository<WoodType, Integer> {
	
	//lista todos os materiais
	List<WoodType> findAll();
}