package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.Company;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Integer> {
	
	//lista todos os materiais
	List<Company> findAll();
	
	@Query(value = "SELECT c FROM Company c WHERE c.id = :id")
	Company findByIdQuery(@Param("id") Integer id);
	
}