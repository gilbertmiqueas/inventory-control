package com.api.inventory.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.ProductionWood;

@Repository
public interface ProductionWoodRepository extends CrudRepository<ProductionWood, Integer> {
	
	//lista todos os materiais
	//List<FixerType> findAll();
}