package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.EntryWood;

@Repository
public interface EntryWoodRepository extends CrudRepository<EntryWood, Integer>{

	//lista todos os materiais
	List<EntryWood> findAll();
}
