package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.Fixer;

@Repository
public interface FixerRepository extends CrudRepository<Fixer, Integer> {
	
	@Query(value = "SELECT f FROM Fixer f WHERE f.size = :size AND f.fixerType.id = :type")
	Fixer findBySpecifications(
			@Param("size") Integer size,
			@Param("type") Integer type);
	
	//lista todos os fixadores
	List<Fixer> findAll();
}
