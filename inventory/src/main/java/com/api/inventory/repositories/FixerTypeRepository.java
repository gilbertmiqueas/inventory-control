package com.api.inventory.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.inventory.models.FixerType;

@Repository
public interface FixerTypeRepository extends CrudRepository<FixerType, Integer> {
	
	//lista todos os materiais
	List<FixerType> findAll();
}