package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.dto.FixerTypeDTO;

public interface FixerTypeService {
	
	//lista todos os materiais
	List<FixerTypeDTO> findAll();
}
