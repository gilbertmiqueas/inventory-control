package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.ExitPallet;
import com.api.inventory.models.dto.ExitPalletDTO;

public interface ExitPalletService {
	
	//lista todos as saidas de pallet
	List<ExitPalletDTO> findAll();
	
	//salva um novo registro de saida de pallet
	public void save(ExitPallet exit);
}
