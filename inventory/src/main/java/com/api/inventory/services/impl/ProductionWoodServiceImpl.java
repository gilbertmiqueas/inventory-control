package com.api.inventory.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.ProductionWood;
import com.api.inventory.repositories.ProductionWoodRepository;
import com.api.inventory.services.ProductionWoodService;

@Service
public class ProductionWoodServiceImpl implements ProductionWoodService{

	//repositories
	@Autowired
	private ProductionWoodRepository productionWoodRepository;
	
	@Override
	public void save(ProductionWood productionWood) {
		productionWoodRepository.save(productionWood);
	}

}
