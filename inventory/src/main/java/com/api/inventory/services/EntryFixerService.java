package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.EntryFixer;
import com.api.inventory.models.dto.EntryFixerDTO;

public interface EntryFixerService {
	
	//lista todos os materiais
	List<EntryFixerDTO> findAll();
	
	//salva um novo registro de entrada de madeira
	public void save(EntryFixer entry);
}

