package com.api.inventory.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.ProductionFixer;
import com.api.inventory.repositories.ProductionFixerRepository;
import com.api.inventory.services.ProductionFixerService;

@Service
public class ProductionFixerServiceImpl implements ProductionFixerService{

	//repositories
	@Autowired
	private ProductionFixerRepository productionFixerRepository;
	
	@Override
	public void save(ProductionFixer productionFixer) {
		productionFixerRepository.save(productionFixer);
	}

}
