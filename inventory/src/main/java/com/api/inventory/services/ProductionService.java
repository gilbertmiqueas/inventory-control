package com.api.inventory.services;

import com.api.inventory.models.ProductionRaw;

public interface ProductionService {
	
	//lista todos os materiais
	//List<EntryWoodDTO> findAll();
	
	//salva um novo registro de producao de pallet
	public void save(ProductionRaw productionRaw);
}
