package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.Wood;
import com.api.inventory.models.dto.WoodDTO;
import com.api.inventory.repositories.WoodRepository;
import com.api.inventory.services.WoodService;

@Service
public class WoodServiceImpl implements WoodService {

	@Autowired
	private WoodRepository woodRepository;
	
	@Autowired
	private BuildDTOServices buildDTOServices;

	@Override
	public Wood save(Wood wood) {
		return woodRepository.save(wood);
	}

	@Override
	public Wood findBySpecification(Integer width, Integer length, Integer thickness, Integer type) {
		return woodRepository.findBySpecifications(width, length, thickness, type);
	}
	
	@Override
	public List<WoodDTO> findAll() {
		
		//procura no banco todas os registros de saida de pallet
		List<Wood> entries = woodRepository.findAll();
		
		//nova lista do tipo DTO
		List<WoodDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (Wood wood: entries)
			entriesDtos.add(buildDTOServices.buildWoodDTO(wood));
		
		return entriesDtos;
	}

}
