package com.api.inventory.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.Fixer;
import com.api.inventory.models.Pallet;
import com.api.inventory.models.Production;
import com.api.inventory.models.ProductionFixer;
import com.api.inventory.models.ProductionRaw;
import com.api.inventory.models.ProductionWood;
import com.api.inventory.models.Wood;
import com.api.inventory.repositories.ProductionRepository;
import com.api.inventory.services.FixerService;
import com.api.inventory.services.PalletService;
import com.api.inventory.services.ProductionFixerService;
import com.api.inventory.services.ProductionService;
import com.api.inventory.services.ProductionWoodService;
import com.api.inventory.services.WoodService;

@Service
public class ProductionServiceImpl implements ProductionService {
	
	//services
	@Autowired
	private WoodService woodService;
	
	@Autowired
	private FixerService fixerService;
	
	@Autowired
	private PalletService palletService;
	
	@Autowired
	private ProductionWoodService productionWoodService;
	
	@Autowired
	private ProductionFixerService productionFixerService;
	
	//repositories
	@Autowired
	private ProductionRepository productionRepository;

	@Override
	public void save(ProductionRaw productionRaw) {
		
		//define a quantidade de pallet producidos neste novo registro
		productionRaw.getProduction().setQuantity(productionRaw.getProduction().getPallet().getQuantity());
		
		//procura no banco por um pallet com as mesmas especificacoes
		Pallet palletDb = palletService.findBySpecification(productionRaw.getProduction().getPallet().getWidth(), 
				productionRaw.getProduction().getPallet().getLength(),
				productionRaw.getProduction().getPallet().getCompany().getId());
		
		//caso ela nao existir no banco
		if (palletDb == null) {
			
			//cadastra no banco
			palletDb = palletService.save(productionRaw.getProduction().getPallet());
		}
		else {
			
			//atualiza a quantidade do pallet
			palletDb.setQuantity(palletDb.getQuantity() + productionRaw.getProduction().getPallet().getQuantity());
			
			//atualiza o pallet no estoque
			palletService.save(palletDb);
		}
		
		//atualiza o pallet do productionRaw
		productionRaw.getProduction().setPallet(palletDb);
		
		//define a data do registro de producao de pallet
		productionRaw.getProduction().setProductionDate(new Date());
		
		//cadastra no banco o novo registro de producao
		Production production = productionRepository.save(productionRaw.getProduction());
		
		
		//lista das madeiras a serem utilizadas na producao
		List<Wood> woodList = productionRaw.getWoodList();
				
		//para cada madeira na lista
		for (Wood wood: woodList) {
			
			//procura no banco por uma madeira com as mesmas especificacoes
			Wood woodDb = woodService.findBySpecification(wood.getWidth(), 
					wood.getLength(), 
					wood.getThickness(), 
					wood.getWoodType().getId());
			
			//atualiza a quantidade da madeira
			woodDb.setQuantity(woodDb.getQuantity() - wood.getQuantity());
			
			//atualiza a madeira no estoque
			woodService.save(woodDb);
			
			//salva a madeira utilizada nasta nova producao de pallet  
			productionWoodService.save(new ProductionWood(wood.getQuantity(), woodDb, production));
		}
		
		
		//lista das madeiras a serem utilizadas na producao
		List<Fixer> fixerList = productionRaw.getFixerList();
		
		//para cada madeira na lista
		for (Fixer fixer: fixerList) {
			
			//procura no banco por um fixador com as mesmas especificacoes
			Fixer fixerDb = fixerService.findBySpecification(fixer.getSize(), 
					fixer.getFixerType().getId());
			
			//atualiza a quantidade do fixador
			fixerDb.setQuantity(fixerDb.getQuantity() - fixer.getQuantity());
			
			//atualiza o fixador no estoque
			fixerService.save(fixerDb);
			
			//salva a madeira utilizada nasta nova producao de pallet  
			productionFixerService.save(new ProductionFixer(fixer.getQuantity(), fixerDb, production));
		}
	}
}
