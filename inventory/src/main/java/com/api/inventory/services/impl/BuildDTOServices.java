package com.api.inventory.services.impl;

import org.springframework.stereotype.Service;

import com.api.inventory.models.Company;
import com.api.inventory.models.CompanyType;
import com.api.inventory.models.EntryFixer;
import com.api.inventory.models.EntryWood;
import com.api.inventory.models.ExitPallet;
import com.api.inventory.models.Fixer;
import com.api.inventory.models.FixerType;
import com.api.inventory.models.Pallet;
import com.api.inventory.models.Wood;
import com.api.inventory.models.WoodType;
import com.api.inventory.models.dto.CompanyDTO;
import com.api.inventory.models.dto.CompanyTypeDTO;
import com.api.inventory.models.dto.EntryFixerDTO;
import com.api.inventory.models.dto.EntryWoodDTO;
import com.api.inventory.models.dto.ExitPalletDTO;
import com.api.inventory.models.dto.FixerDTO;
import com.api.inventory.models.dto.FixerTypeDTO;
import com.api.inventory.models.dto.PalletDTO;
import com.api.inventory.models.dto.WoodDTO;
import com.api.inventory.models.dto.WoodTypeDTO;

@Service
public class BuildDTOServices {

	public EntryWoodDTO buildEntryWoodDTO(EntryWood entry) {
		return new EntryWoodDTO(entry.getId(), entry.getQuantity(), buildWoodDTO(entry.getWood()), buildCompanyDTO(entry.getCompany()), entry.getEntryDate());
	}
	
	public WoodDTO buildWoodDTO(Wood wood) {
		return new WoodDTO(wood.getId(), wood.getWidth(), wood.getLength(), wood.getThickness(), wood.getQuantity(), buildWoodTypeDTO(wood.getWoodType()));
	}

	public WoodTypeDTO buildWoodTypeDTO(WoodType woodType) {
		return new WoodTypeDTO(woodType.getId(), woodType.getName(), null);
	}
	
	public EntryFixerDTO buildEntryFixerDTO(EntryFixer entry) {
		return new EntryFixerDTO(entry.getId(), entry.getQuantity(), buildFixerDTO(entry.getFixer()), buildCompanyDTO(entry.getCompany()), entry.getEntryDate());
	}

	public FixerDTO buildFixerDTO(Fixer fixer) {
		return new FixerDTO(fixer.getId(), fixer.getSize(), fixer.getQuantity(), buildFixerTypeDTO(fixer.getFixerType()));
	}

	public FixerTypeDTO buildFixerTypeDTO(FixerType fixerType) {
		return new FixerTypeDTO(fixerType.getId(), fixerType.getName(), null);
	}
	
	public CompanyDTO buildCompanyDTO(Company company) {
		return new CompanyDTO(company.getId(), company.getName(), buildCompanyTypeDTO(company.getCompanyType()));
	}

	public CompanyTypeDTO buildCompanyTypeDTO(CompanyType companyType) {
		return new CompanyTypeDTO(companyType.getId(), companyType.getName(), null);
	}
	
	public ExitPalletDTO buildExitPalletDTO(ExitPallet exit) {
		return new ExitPalletDTO(exit.getId(), exit.getQuantity(), buildPalletDTO(exit.getPallet()), exit.getExitDate());
	}
	
	public PalletDTO buildPalletDTO(Pallet pallet) {
		return new PalletDTO(pallet.getId(), pallet.getWidth(), pallet.getLength(), pallet.getQuantity(), buildCompanyDTO(pallet.getCompany()));
	}

}
