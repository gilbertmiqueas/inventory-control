package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.Wood;
import com.api.inventory.models.dto.WoodDTO;

public interface WoodService {
	
	public Wood save(Wood wood);
	
	public Wood findBySpecification(Integer width, Integer length, Integer thickness, Integer type);
	
	//lista todos as madeiras
	List<WoodDTO> findAll();
}
