package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.Fixer;
import com.api.inventory.models.dto.FixerDTO;
import com.api.inventory.repositories.FixerRepository;
import com.api.inventory.services.FixerService;

@Service
public class FixerServiceImpl implements FixerService {

	@Autowired
	private FixerRepository fixerRepository;
	
	@Autowired
	private BuildDTOServices buildDTOServices;

	@Override
	public Fixer save(Fixer fixer) {
		return fixerRepository.save(fixer);
	}

	@Override
	public Fixer findBySpecification(Integer size, Integer type) {
		return fixerRepository.findBySpecifications(size, type);
	}

	@Override
	public List<FixerDTO> findAll() {
		
		//procura no banco todas os fixadores
		List<Fixer> entries = fixerRepository.findAll();
		
		//nova lista do tipo DTO
		List<FixerDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (Fixer fixer: entries)
			entriesDtos.add(buildDTOServices.buildFixerDTO(fixer));
		
		return entriesDtos;
	}
}