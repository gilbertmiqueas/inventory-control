package com.api.inventory.services;

import com.api.inventory.models.ProductionFixer;

public interface ProductionFixerService {
	
	//lista todos os materiais
	//List<EntryWoodDTO> findAll();
	
	//salva um novo registro das madeiras utilizadas na producao de pallet
	public void save(ProductionFixer productionFixer);
}