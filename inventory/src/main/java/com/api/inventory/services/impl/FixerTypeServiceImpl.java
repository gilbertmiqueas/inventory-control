package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.FixerType;
import com.api.inventory.models.dto.FixerTypeDTO;
import com.api.inventory.repositories.FixerTypeRepository;
import com.api.inventory.services.FixerTypeService;

@Service
public class FixerTypeServiceImpl implements FixerTypeService {
	
	//repositories
	@Autowired
	private FixerTypeRepository fixerTypeRepository;
	
	@Autowired
	private BuildDTOServices buildDTOServices;

	@Override
	public List<FixerTypeDTO> findAll() {
		
		//procura no banco todas os registros de saida de pallet
		List<FixerType> entries = fixerTypeRepository.findAll();
		
		//nova lista do tipo DTO
		List<FixerTypeDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (FixerType fixerType: entries)
			entriesDtos.add(buildDTOServices.buildFixerTypeDTO(fixerType));
		
		return entriesDtos;
	}

}
