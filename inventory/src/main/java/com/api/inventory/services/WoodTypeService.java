package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.dto.WoodTypeDTO;

public interface WoodTypeService {
	
	//lista todos os materiais
	List<WoodTypeDTO> findAll();
}
