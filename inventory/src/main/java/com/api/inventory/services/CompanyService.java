package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.Company;
import com.api.inventory.models.dto.CompanyDTO;

public interface CompanyService {

	Company findById(Integer id);
	
	//lista todos os materiais
	List<CompanyDTO> findAll();
}
