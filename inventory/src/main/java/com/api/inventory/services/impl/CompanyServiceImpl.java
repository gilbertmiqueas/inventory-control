package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.Company;
import com.api.inventory.models.dto.CompanyDTO;
import com.api.inventory.repositories.CompanyRepository;
import com.api.inventory.services.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	//repositories
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private BuildDTOServices buildDTOServices;

	@Override
	public Company findById(Integer id) {
		return companyRepository.findByIdQuery(id);
	}

	@Override
	public List<CompanyDTO> findAll() {

		//procura no banco todas os registros de saida de pallet
		List<Company> entries = companyRepository.findAll();
		
		//nova lista do tipo DTO
		List<CompanyDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (Company company: entries)
			entriesDtos.add(buildDTOServices.buildCompanyDTO(company));
		
		return entriesDtos;
	}
}
