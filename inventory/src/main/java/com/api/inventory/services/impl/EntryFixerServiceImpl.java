package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.EntryFixer;
import com.api.inventory.models.Fixer;
import com.api.inventory.models.dto.EntryFixerDTO;
import com.api.inventory.repositories.EntryFixerRepository;
import com.api.inventory.services.CompanyService;
import com.api.inventory.services.EntryFixerService;
import com.api.inventory.services.FixerService;

@Service
public class EntryFixerServiceImpl implements EntryFixerService {
	
	//services
	@Autowired
	private  FixerService fixerService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private BuildDTOServices buildDTOServices;
	
	//repositories
	@Autowired
	private EntryFixerRepository entryFixerRepository;

	@Override
	public void save(EntryFixer entry) {
		
		//define a quantidade de madeira desta entrada
		entry.setQuantity(entry.getFixer().getQuantity());
		
		//procura no banco por uma madeira com as mesmas especificacoes
		Fixer fixerDb = fixerService.findBySpecification(entry.getFixer().getSize(), entry.getFixer().getFixerType().getId());
		
		//caso ela nao existir no banco
		if (fixerDb == null) {
			
			//cadastra no banco
			fixerDb = fixerService.save(entry.getFixer());
		}
		else {
			
			//atualiza a quantidade da madeira
			fixerDb.setQuantity(fixerDb.getQuantity() + entry.getFixer().getQuantity());
			
			//atualiza a madeira no estoque
			fixerService.save(fixerDb);
		}
		
		//define a madeira deste registro de entrada de madeira
		entry.setFixer(fixerDb);
		
		//define a empresa fornecedora da madeira
		entry.setCompany(companyService.findById(entry.getCompany().getId()));
		
		//define a data do registro de entrada de madeira
		entry.setEntryDate(new Date());
		
		//cadastra este registro de entrada de madeira
		entryFixerRepository.save(entry);
	}

	@Override
	public List<EntryFixerDTO> findAll() {
		
		//procura no banco todas os registros de entrada de madeira
		List<EntryFixer> entries = entryFixerRepository.findAll();
		
		//nova lista do tipo DTO
		List<EntryFixerDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (EntryFixer entry: entries)
			entriesDtos.add(buildDTOServices.buildEntryFixerDTO(entry));
		
		return entriesDtos;
	}
}
