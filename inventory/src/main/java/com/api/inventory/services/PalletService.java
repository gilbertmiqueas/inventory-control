package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.Pallet;
import com.api.inventory.models.dto.PalletDTO;

public interface PalletService {
	
	//salva um novo pallet ou atualiza a quantidade de um ja existente
	public Pallet save(Pallet pallet);
	
	public Pallet findBySpecification(Integer width, Integer length, Integer company);
	
	//lista todos os pallets
	List<PalletDTO> findAll();
}
