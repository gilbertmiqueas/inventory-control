package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.Fixer;
import com.api.inventory.models.dto.FixerDTO;

public interface FixerService {
	
	public Fixer save(Fixer fixer);
	
	public Fixer findBySpecification(Integer size, Integer type);
	
	//lista todos os fixadores
	List<FixerDTO> findAll();
}
