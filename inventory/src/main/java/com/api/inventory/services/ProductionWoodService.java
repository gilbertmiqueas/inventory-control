package com.api.inventory.services;

import com.api.inventory.models.ProductionWood;

public interface ProductionWoodService {
	
	//lista todos os materiais
	//List<EntryWoodDTO> findAll();
	
	//salva um novo registro das madeiras utilizadas na producao de pallet
	public void save(ProductionWood productionWood);
}
