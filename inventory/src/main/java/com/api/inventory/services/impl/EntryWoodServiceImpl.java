package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.EntryWood;
import com.api.inventory.models.Wood;
import com.api.inventory.models.dto.EntryWoodDTO;
import com.api.inventory.repositories.EntryWoodRepository;
import com.api.inventory.services.CompanyService;
import com.api.inventory.services.EntryWoodService;
import com.api.inventory.services.WoodService;

@Service
public class EntryWoodServiceImpl implements EntryWoodService {
	
	//services
	@Autowired
	private WoodService woodService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private BuildDTOServices buildDTOServices;
	
	//repositories
	@Autowired
	private EntryWoodRepository entryWoodRepository;

	@Override
	public void save(EntryWood entry) {
		
		//define a quantidade de madeira desta entrada
		entry.setQuantity(entry.getWood().getQuantity());
		
		//procura no banco por uma madeira com as mesmas especificacoes
		Wood woodDb = woodService.findBySpecification(entry.getWood().getWidth(), 
				entry.getWood().getLength(), 
				entry.getWood().getThickness(), 
				entry.getWood().getWoodType().getId());
		
		//caso ela nao existir no banco
		if (woodDb == null) {
			
			//cadastra no banco
			woodDb = woodService.save(entry.getWood());
		}
		else {
			
			//atualiza a quantidade da madeira
			woodDb.setQuantity(woodDb.getQuantity() + entry.getWood().getQuantity());
			
			//atualiza a madeira no estoque
			woodService.save(woodDb);
		}
		
		//define a madeira deste registro de entrada de madeira
		entry.setWood(woodDb);
		
		//define a empresa fornecedora da madeira
		entry.setCompany(companyService.findById(entry.getCompany().getId()));
		
		//define a data do registro de entrada de madeira
		entry.setEntryDate(new Date());
		
		//cadastra este registro de entrada de madeira
		entryWoodRepository.save(entry);
	}

	@Override
	public List<EntryWoodDTO> findAll() {
		
		//procura no banco todas os registros de entrada de madeira
		List<EntryWood> entries = entryWoodRepository.findAll();
		
		//nova lista do tipo DTO
		List<EntryWoodDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (EntryWood entry: entries)
			entriesDtos.add(buildDTOServices.buildEntryWoodDTO(entry));
		
		return entriesDtos;
	}
}
