package com.api.inventory.services;

import java.util.List;

import com.api.inventory.models.EntryWood;
import com.api.inventory.models.dto.EntryWoodDTO;

public interface EntryWoodService {
	
	//lista todos os materiais
	List<EntryWoodDTO> findAll();
	
	//salva um novo registro de entrada de madeira
	public void save(EntryWood entry);
}
