package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.ExitPallet;
import com.api.inventory.models.Pallet;
import com.api.inventory.models.dto.ExitPalletDTO;
import com.api.inventory.repositories.ExitPalletRepository;
import com.api.inventory.services.ExitPalletService;
import com.api.inventory.services.PalletService;

@Service
public class ExitPalletServiceImpl implements ExitPalletService {
	
	//services
	@Autowired
	private PalletService palletService;
	
	@Autowired
	private BuildDTOServices buildDTOServices;
	
	//repositories
	@Autowired
	private ExitPalletRepository exitPalletRepository;

	@Override
	public void save(ExitPallet exit) {
		
		//procura no banco por um pallet com as mesmas especificacoes
		Pallet palletDb = palletService.findBySpecification(exit.getPallet().getWidth(), 
				exit.getPallet().getLength(), 
				exit.getPallet().getCompany().getId());
		
		//caso o pallet nao existir no banco
		if (palletDb == null) {
			
			//cancela a acao
		}
		else {
			
			// nova quantidade deste pallet apos a saida
			Integer newQuantity = palletDb.getQuantity() - exit.getQuantity();
			
			// caso a nova quantidade for negativa
			if(newQuantity < 0) {
				
				//cancela a acao
			}
				
			//atualiza a quantidade da pallet
			palletDb.setQuantity(newQuantity);
			
			//atualiza o pallet no estoque
			palletService.save(palletDb);
		}
		
		//define o pallet deste registro de saida de pallet
		exit.setPallet(palletDb);
		
		//define a data do registro de saida de pallet
		exit.setExitDate(new Date());
		
		//cadastra este registro de saida de pallet
		exitPalletRepository.save(exit);
	}

	@Override
	public List<ExitPalletDTO> findAll() {
		
		//procura no banco todas os registros de saida de pallet
		List<ExitPallet> entries = exitPalletRepository.findAll();
		
		//nova lista do tipo DTO
		List<ExitPalletDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (ExitPallet exit: entries)
			entriesDtos.add(buildDTOServices.buildExitPalletDTO(exit));
		
		return entriesDtos;
	}
}
