package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.WoodType;
import com.api.inventory.models.dto.WoodTypeDTO;
import com.api.inventory.repositories.WoodTypeRepository;
import com.api.inventory.services.WoodTypeService;

@Service
public class WoodTypeServiceImpl implements WoodTypeService {
	
	//repositories
	@Autowired
	private WoodTypeRepository woodTypeRepository;
	
	@Autowired
	private BuildDTOServices buildDTOServices;

	@Override
	public List<WoodTypeDTO> findAll() {
		
		//procura no banco todas os registros de saida de pallet
		List<WoodType> entries = woodTypeRepository.findAll();
		
		//nova lista do tipo DTO
		List<WoodTypeDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (WoodType woodType: entries)
			entriesDtos.add(buildDTOServices.buildWoodTypeDTO(woodType));
		
		return entriesDtos;
	}

}
