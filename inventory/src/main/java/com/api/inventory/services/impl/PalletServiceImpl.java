package com.api.inventory.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.inventory.models.Pallet;
import com.api.inventory.models.dto.PalletDTO;
import com.api.inventory.repositories.PalletRepository;
import com.api.inventory.services.PalletService;

@Service
public class PalletServiceImpl implements PalletService {

	@Autowired
	private PalletRepository palletRepository;
	
	@Autowired
	private BuildDTOServices buildDTOServices;

	@Override
	public Pallet save(Pallet pallet) {
		return palletRepository.save(pallet);
	}

	@Override
	public Pallet findBySpecification(Integer width, Integer length, Integer company) {
		return palletRepository.findBySpecifications(width, length, company);
	}
	
	@Override
	public List<PalletDTO> findAll() {
		
		//procura no banco todas os registros de saida de pallet
		List<Pallet> entries = palletRepository.findAll();
		
		//nova lista do tipo DTO
		List<PalletDTO> entriesDtos = new ArrayList<>();
		
		//conversao da lista para DTO
		for (Pallet pallet: entries)
			entriesDtos.add(buildDTOServices.buildPalletDTO(pallet));
		
		return entriesDtos;
	}

}
